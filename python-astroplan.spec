%global _empty_manifest_terminate_build 0
Name:		python-astroplan
Version:	0.8
Release:	1
Summary:	Observation planning package for astronomers
License:	BSD
URL:		https://github.com/astropy/astroplan
Source0:	https://files.pythonhosted.org/packages/49/23/15a8bcc0902c8073857995d0db933c5b86937e0a758152c2ea9b46e70a74/astroplan-0.8.tar.gz
BuildArch:	noarch


%description
astroplan is an observation planning package for 
astronomers that can help you plan for everything but the clouds.

%package -n python3-astroplan
Summary:	Observation planning package for astronomers
Provides:	python-astroplan
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-setuptools_scm
%description -n python3-astroplan
astroplan is an observation planning package for 
astronomers that can help you plan for everything but the clouds.

%package help
Summary:	Development documents and examples for astroplan
Provides:	python3-astroplan-doc
%description help
astroplan is an observation planning package for 
astronomers that can help you plan for everything but the clouds.

%prep
%autosetup -n astroplan-0.8

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-astroplan -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Sep 06 2021 Python_Bot <Python_Bot@openeuler.org> - 0.8-1
- Package Init
